
/* Includes */
#include <stddef.h>
#include <stdlib.h>
#include "stm32f10x.h"
#include "Lenux/Lenux.h"

static void StartupTask(unsigned int Parameter)
{
	volatile uint8_t X[16] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10};

	while(1)
	{
		SwitchTask();
		GPIOA->BSRR = 0x00000001;
		SwitchTask();
		GPIOA->BSRR = 0x00010000;
	}

	return;
}


static void SecondTask(unsigned int Parameter)
{

	while(1)
	{
		SwitchTask();
		GPIOA->BSRR = 0x00000002;
		SwitchTask();
		GPIOA->BSRR = 0x00020000;
	}
}

int main(void)
{

	// Set the following:
	// MCO = 0b000	(No clock)
	// USBPRE = 0 (USB clock divided by 1.5 to give 48 MHz)
	// PLLMUL = 0b0111 (multiply by 9, SYSCLK = 72 MHz)
	// PLLXTPRE = 0 (clock not divided)
	// PLLSRC = 1 (HSE clock selected)
	// ADCPRE = 0b11 (ADC clock divided by 8, = 9 MHz)
	// PPRE2 = 0b000 (Clock not divided, APB2 = 72 MHz)
	// PPRE1 = 0b100 (Clock divided by 2, APB1 = 36 MHz)
	// HPRE = 0b0000 (clock not divided, HCLK = 72 MHz)
	// SW = 0b10 (PLL selected as system clock)
	RCC->CFGR = 0x001DC402;

	// Set the following:
	// PLLON = 1
	// CSSON = 1
	// HSEBYP = 0
	// HSEON = 1
	// HSITRIM = 0b10000
	// HSION = 1
	RCC->CR = 0x01090083;

	//Enable clock for IO port A
	RCC->APB2ENR |= 0x00000004;
	//Set PA0 and PA1 to push/pull outputs, the rest are inputs
	GPIOA->CRL = 0x44444422;

	InitLenux();

	CreateTask(StartupTask, 0x12345678);
	CreateTask(SecondTask, 0);
	MultiTask();
}
