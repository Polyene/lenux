.cpu cortex-m3
.thumb
.syntax unified

.extern LenuxGetContextStorage
.extern LenuxGetCurrentTaskContext
.extern LenuxSwitchTask

.text
.global SwitchTask
SwitchTask:
	PUSH 	{LR}
	MOV 	r0, SP
	BL		LenuxGetContextStorage
	STM		R0!, {R4-R12}

SaveStackAndResume:
	LDR		R2, =_estack
	MOV		R1, SP
	SUB		R1, R2, R1

	BICS	R4, R1, #31
	BEQ		No32BytesStore
Store32Bytes:
	LDM		SP!, {R4-R11}
	SUB		R1, #32
	STM		R0!, {R4-R11}

	BICS	R4, R1, #31
	BNE		Store32Bytes

No32BytesStore:

	BICS	R4, R1, #15
	BEQ		No16BytesStore

	LDM		SP!, {R4-R7}
	SUB		R1, #16
	STM		R0!, {R4-R7}

No16BytesStore:

	BICS	R4, R1, #7
	BEQ		No8BytesStore

	LDM		SP!, {R4-R5}
	SUB		R1, #8
	STM		R0!, {R4-R5}

No8BytesStore:

	CBZ		R1, StackStored

Store4Bytes:
	LDM		SP!, {R4}
	STM		R0!, {R4}

StackStored:

	SUB		SP, #8
	MOV 	R0, SP
	BL		LenuxSwitchTask

ResumeTask:
	POP 	{r2-r3}

	SUB		R1,SP,R3
	MOV		SP, R3

	ADD		R0, R2, #36


	BICS	R4, R1, #31
	BEQ		No32BytesFill

Fill32Bytes:
	LDM		R0!, {R4-R11}
	SUB		R1, #32
	STM		R3!, {R4-R11}

	BICS	R4, R1, #31
	BNE		Fill32Bytes

No32BytesFill:

	BICS	R4, R1, #15
	BEQ		No16BytesFill

	LDM		R0!, {R4-R7}
	SUB		R1, #16
	STM		R3!, {R4-R7}

No16BytesFill:

	BICS	R4, R1, #7
	BEQ		No8BytesFill

	LDM		R0!, {R4-R5}
	SUB		R1, #8
	STM		R3!, {R4-R5}

No8BytesFill:

	CBZ		R1, StackFilled

Fill4Bytes:
	LDM		R0!, {R4}
	STM		R3!, {R4}

StackFilled:
	LDM		R2!, {R4-R12}

	POP 	{PC}

.global LenuxPipeWait
LenuxPipeWait:
	PUSH 	{R0, LR}
	ADD		R0, SP, #4
	BL 		LenuxGetContextStorage
	STM		R0!, {R4-R12}

	POP		{R3}
	LDR		R2,[R3]
	CMP		SP, R2
	BHS		SaveStackAndResume
	LDR		R1, =_estack
	CMP		R2, R1
	BHS		SaveStackAndResume

	MOV		R1, SP
	SUB		R2, R2, R1 	//Here the relocation is done, changing the pointer to point to the task's context storage instead of to it's stack
	ADD		R2, R0
	STR		R2,[R3]
	B		SaveStackAndResume

.global MultiTask
MultiTask:
	LDR 	SP, =_estack
	SUB 	SP, #8
	MOV 	R0, SP
	BL		LenuxGetCurrentTaskContext
	B		ResumeTask

.global LenuxLoadParameter
LenuxLoadParameter:
	POP		{R0, R1}
	POP 	{LR}
	MOV 	PC,R1

.global LenuxPrepareStackForProfiling

LenuxPrepareStackForProfiling:

	MOV 	PC, LR
