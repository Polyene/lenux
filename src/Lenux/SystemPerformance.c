#include "SystemPerformance.h"
#include "PIC24OS.h"
#include "../CANOpenSDOServer.h"
#include "../Hardware/EEPROM.h"
#include "../EEPROMStructure.h"
#include <stdlib.h>

typedef struct _PerformanceData
{
	unsigned long Counter;
	unsigned short MaxTime;
	unsigned long Counter1Sec;
}PerformanceData;

static PerformanceData Performance;

static void PerformanceSDOServer(unsigned int Method,CANOpenSDOObjectInfo *pObject)
{
	PIC24OSPerformance OSPerformance;
	CANOpenSDOServerPerformance SDOServerPerformance;
	
	if(Method == SDO_OBJECT_GET)
	{
		if(pObject->Index == 0x2106)
		{
			pObject->AccessType = ACCESSTYPE_RO;
			pObject->Size = 2;
			pObject->Found = 1;
			
			if(pObject->SubIndex >= 4)
			{
				if(pObject->SubIndex <= 10)
					QueryPIC24OSPerformance(&OSPerformance);
				else if(pObject->SubIndex <= 13)
					QueryCANOpenSDOServerPerformance(&SDOServerPerformance);
			}
			
			switch(pObject->SubIndex)
			{
			case 0:
				pObject->Data = 17;
				pObject->Size = 1;
				break;
			case 1:
				pObject->Pointer = &Performance.Counter;
				pObject->Size = 4;
				break;
			case 2:
				pObject->Pointer = &Performance.MaxTime;
				break;
			case 3:
				pObject->Pointer = &Performance.Counter1Sec;
				pObject->Size = 4;
				break;
			case 4:
				*((unsigned short*)&pObject->Data) = OSPerformance.TotalNumberOfTasks;
				break;
			case 5:
				*((unsigned short*)&pObject->Data) = OSPerformance.AllocatedTaskListSize;
				break;
			case 6:
				*((unsigned short*)&pObject->Data) = OSPerformance.NumberOfSuspendedTasks;
				break;
			case 7:
				*((unsigned short*)&pObject->Data) = OSPerformance.NumberOfActivePipes;
				break;
			case 8:
				*((unsigned short*)&pObject->Data) = OSPerformance.TotalAllocatedContextStorage;
				break;
			case 9:
				*((unsigned short*)&pObject->Data) = OSPerformance.TotalUsedContextStorage;
				break;
			case 10:
				*((unsigned short*)&pObject->Data) = OSPerformance.MaximumStackUsage;
				break;
			case 11:
				*((unsigned short*)&pObject->Data) = SDOServerPerformance.NumberOfRegisteredSDOHandlers;
				break;
			case 12:
				*((unsigned short*)&pObject->Data) = SDOServerPerformance.NumberOfSuccessfulSDOServices;
				break;
			case 13:
				*((unsigned short*)&pObject->Data) = SDOServerPerformance.NumberOfAbortedSDOServices;
				break;
			case 14:
				pObject->Data = sizeof(EEPROMStructure);
				pObject->Size = 4;
				break;
			case 15:
				pObject->Data = GetEEPROMSize();
				pObject->Size = 4;
				break;
			case 16:
			case 17:
			case 18:
				pObject->Found = 0;
				break;
			case 19:
				*((unsigned short*)&pObject->Data) = GetEEPROMJournalSize();
				break;
			case 20:
				pObject->Pointer = CreatePIC24OSProfilingInformation(&(pObject->Size));
				break;
			default:
				pObject->Found = 0;
				break;
			}
		}
	}
	else if(Method == SDO_OBJECT_FINISHED)
	{
		if(pObject->SubIndex == 20)
			free(pObject->Pointer);
	}
	return;
}

void SystemPerformanceTask(unsigned int Parameter)
{
	unsigned long Counter,Counter1Sec;
	unsigned short MaxTime;
	unsigned short PeriodTimer,Timer1Sec;
	unsigned short Timer,Time;
	
	RegisterCANOpenSDOObjectHandler(PerformanceSDOServer);
	
	Performance.Counter = 0;
	Performance.MaxTime = 0;
	Performance.Counter1Sec = 0;
	Counter = 0;	
	MaxTime = 0;
	Counter1Sec = 0;

	ResetTimer(&PeriodTimer);
	ResetTimer(&Timer);
	ResetTimer(&Timer1Sec);
	while(1)
	{
		++Counter;
		++Counter1Sec;
		
		Time = GetTimeSinceLastCheck(&Timer);
		if(Time > MaxTime)
			MaxTime = Time;
		
		if(Timeout(&PeriodTimer,10000))
		{
			Performance.Counter = Counter;
			Performance.MaxTime = MaxTime;
			MaxTime = 0;
			Counter = 0;
		}
		
		if(Timeout(&Timer1Sec,1000))
		{
			Performance.Counter1Sec = Counter1Sec;
			Counter1Sec = 0;
		}
		SwitchTask();
	}
}
