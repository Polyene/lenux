#include "Lenux.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "stm32f10x.h"

#define SAVED_CONTEXT_SIZE	(9 * sizeof(uint32_t))

#define TASKFLAG_SUSPENDED			0x0001
#define TASKFLAG_EVENT_SIGNALED		0x0002
#define TASKFLAG_PRIORITIZED        0x0004
#define TASKFLAG_WAITING_EVENT      0x0008

typedef struct _LenuxTaskProfiling
{
	Task pEntryPoint;
	HighResolutionTimer_t LastRunTime;
	HighResolutionTimer_t MaxRunTime;
} LenuxTaskProfiling;

typedef struct _LenuxTask LenuxTask;

typedef struct _LenuxEvent
{
	LenuxTask *pTask;
	int (*EventFunction)(void);
	struct _LenuxEvent *pNext;
} LenuxEvent;

typedef struct _LenuxTask
{
	uint32_t *pContextStorage;
	uint16_t Flags;
	uint32_t ContextSize;
	uint32_t StackPointer;
	LenuxTaskProfiling Profiling;
	LenuxEvent WaitEvent;
}LenuxTask;

typedef struct _LenuxPipe
{
	void *Destination;	//Destination needs to be first, since it is altered by assembly code
	enum PipeIdentifiers Identifier;
	uint32_t Size;
	unsigned int Time;
	Timer_t Timer;
	LenuxTask *pTask;
	struct _LenuxPipe *Next;
}LenuxPipe;

typedef struct _Lenux
{
	LenuxTask **TaskList;
	LenuxTask **PriorityTasks;
	LenuxPipe *Pipes;
	LenuxPipe *pAllocatedPipes[LastPipeIdentifier];
	LenuxEvent *pEvents;
	uint32_t NumberOfTasks,AllocatedTaskList;
	uint32_t AllocatedPriorityList;
	uint32_t CurrentTaskNumber, RoundRobinTaskNumber;
	HighResolutionTimer_t Timer;
}Lenux;

typedef struct _LenuxSwitchTaskReturn
{
	uint32_t *pContextStorage;
	uint32_t StackPointer;
}LenuxSwitchTaskReturn;

uint32_t* LenuxGetContextStorage(uint32_t StackPointer);
LenuxSwitchTaskReturn LenuxSwitchTask();
LenuxSwitchTaskReturn LenuxGetCurrentTaskContext();
unsigned short LenuxLoadParameter();
void LenuxPipeWait(LenuxPipe *Pipe);
void LenuxPrepareStackForProfiling();
uint32_t LenuxGetMaximumStackUsage();
void InitLenuxTimer();
static int LenuxSetPrioritizedTask(LenuxTask *pTask);

static Lenux OS;
static void LenuxDeleteCurrentTask();
static HighResolutionTimer_t ReadTimerHighResolution();


void QueryLenuxPerformance(LenuxPerformance *pPerformance)
{
	LenuxTask *pTask;
	LenuxPipe *pPipe;
	unsigned short Index;
	
	pPerformance->TotalNumberOfTasks = OS.NumberOfTasks;
	pPerformance->AllocatedTaskListSize = OS.AllocatedTaskList;
	pPerformance->NumberOfSuspendedTasks = 0;
	pPerformance->NumberOfActivePipes = 0;
	pPerformance->TotalAllocatedContextStorage = 0;
	pPerformance->TotalUsedContextStorage = 0;
	
	for(Index = 0 ; Index < OS.NumberOfTasks ; ++Index)
	{
		pTask = *(OS.TaskList + Index);
		if(pTask->Flags & TASKFLAG_SUSPENDED)
			pPerformance->NumberOfSuspendedTasks++;
		pPerformance->TotalAllocatedContextStorage += pTask->ContextSize;

// TODO: Calculate each task's currently used context size and add it to the total
//		pPerformance->TotalUsedContextStorage += pTask->StackPointer - ((unsigned short)&_SP_init) + SAVED_CONTEXT_SIZE;
	}
	for(pPipe = OS.Pipes ; pPipe ; pPipe = pPipe->Next)
		pPerformance->NumberOfActivePipes++;

//TODO: Stack usage profiling
//	pPerformance->MaximumStackUsage = LenuxGetMaximumStackUsage();
	pPerformance->MaximumStackUsage = 0;
	return;
}


void* CreateLenuxProfilingInformation(size_t *pSize)
{
	LenuxTaskProfiling *pResult;
	unsigned short Index;
	
	*pSize = OS.NumberOfTasks * sizeof(LenuxTaskProfiling);
	
	if((pResult = malloc(*pSize)) != NULL)
	{
		for(Index = 0 ; Index < OS.NumberOfTasks ; ++Index)
		{
			memcpy(pResult + Index, &((*(OS.TaskList + Index))->Profiling),sizeof(LenuxTaskProfiling));
			(*(OS.TaskList + Index))->Profiling.MaxRunTime.DWORD = 0;
		}
	}
	return pResult;
}

extern uint32_t _estack;

int CreateTaskEx(Task pTaskEntry,unsigned int Parameter,int ImmediateInvoke)
{
	LenuxTask *pTask;
	
	if((OS.NumberOfTasks + 1) > OS.AllocatedTaskList)
	{
		LenuxTask **NewTaskList;
		NewTaskList = malloc((OS.NumberOfTasks + 1) * sizeof(LenuxTask**));
		
		if(NewTaskList == NULL)
			return -1;
		
		memcpy(NewTaskList,OS.TaskList,OS.NumberOfTasks * sizeof(LenuxTask**));
		free(OS.TaskList);
		
		OS.TaskList = NewTaskList;
		OS.AllocatedTaskList = OS.NumberOfTasks + 1;
	}
	pTask = malloc(sizeof(LenuxTask));
	if(pTask == NULL)
		return -2;
	
	pTask->pContextStorage = malloc(INITIAL_CONTEXT_STORAGE_SIZE);
	if(pTask->pContextStorage == NULL)
	{
		free(pTask);
		return -3;
	}
	
	*(OS.TaskList + OS.NumberOfTasks) = pTask;	
	++OS.NumberOfTasks;
	
	pTask->ContextSize = INITIAL_CONTEXT_STORAGE_SIZE;
	pTask->Flags = 0x0000;
	memset(&(pTask->Profiling),0x00, sizeof(LenuxTaskProfiling));
	pTask->Profiling.pEntryPoint = pTaskEntry;

	*(pTask->pContextStorage + 12) = ((uint32_t)LenuxDeleteCurrentTask) | 1;
	*(pTask->pContextStorage + 11) = ((uint32_t)pTaskEntry) | 1;
	*(pTask->pContextStorage + 10) = Parameter;
	*(pTask->pContextStorage + 9) = ((uint32_t)LenuxLoadParameter) | 1;
	*(pTask->pContextStorage + 8) = 9; // R12
	*(pTask->pContextStorage + 7) = 8; // R11
	*(pTask->pContextStorage + 6) = 7; // R10
	*(pTask->pContextStorage + 5) = 6; // R9
	*(pTask->pContextStorage + 4) = 5; // R8
	*(pTask->pContextStorage + 3) = 4; // R7
	*(pTask->pContextStorage + 2) = 3; // R6
	*(pTask->pContextStorage + 1) = 2; // R5
	*(pTask->pContextStorage + 0) = 1; // R4
	pTask->StackPointer = (uint32_t)(&_estack - 4);

	if(ImmediateInvoke)
	{
		LenuxSetPrioritizedTask(pTask);
		SwitchTask();
	}
	return 1;
}

int CreateTask(Task pTaskEntry,unsigned int Parameter)
{
	return CreateTaskEx(pTaskEntry,Parameter,0);
}

uint32_t* LenuxGetContextStorage(uint32_t StackPointer)
{
	uint32_t ContextSize;
	LenuxTask *pTask;
	HighResolutionTimer_t Now,RunTime;
	
	pTask = *(OS.TaskList + OS.CurrentTaskNumber);
	
	pTask->StackPointer = StackPointer;
	
	ContextSize = ((uint32_t)&_estack) - StackPointer + SAVED_CONTEXT_SIZE;
	
	if(ContextSize > pTask->ContextSize)
	{
		if(pTask->pContextStorage != NULL)
			free(pTask->pContextStorage);
		pTask->pContextStorage = malloc(ContextSize);
		if(pTask->pContextStorage != NULL)
		{
			pTask->ContextSize = ContextSize;
		}
		else
		{
			/* 	If we land here, we're in trouble. Not enough memory to store the
			 * 	task context.
			 *
			 * 	One alternative would be to loop over all other tasks, and realloc
			 * 	their context storages to the exact sizes that they need at the
			 * 	moment, and then try to allocate this task's storage once again.
			 *
			 * 	If that would not help, there are two options. We either kill the
			 * 	current task, and try to notify the user code about this somehow,
			 * 	or we skip this context switch and let this task keep running, but
			 * 	this may cause worse problems.
			 *
			 * 	Note - If you would realloc the context storage of a task that is
			 * 	listening in a pipe, you'd also need to relocate the pipe's
			 * 	destination.
			 */
			
			pTask->ContextSize = 0;
		}
	}
	

	Now = ReadTimerHighResolution();
	RunTime.High = Now.High - OS.Timer.High;
	if(Now.Low < OS.Timer.Low)
	{
		RunTime.High -= 1;
		Now.Low += (SYSTEM_FOSC / 1000);
	}
	RunTime.Low = Now.Low - OS.Timer.Low;
	pTask->Profiling.LastRunTime = RunTime;

	if(RunTime.DWORD > pTask->Profiling.MaxRunTime.DWORD)
		pTask->Profiling.MaxRunTime = RunTime;
	
	return pTask->pContextStorage;
}

static int SchedulePrioritizedTask()
{
	LenuxTask *pTask;
    size_t Index;
	unsigned short Search;
    
    if(OS.PriorityTasks[0] != NULL)
    {
        for(Index = 0 ; Index < OS.AllocatedPriorityList ; ++Index)
        {
            if(OS.PriorityTasks[Index] == NULL)
            {
                --Index;
                break;
            }
        }

        pTask = OS.PriorityTasks[Index];
        OS.PriorityTasks[Index] = NULL;

        pTask->Flags &= ~(TASKFLAG_EVENT_SIGNALED | TASKFLAG_PRIORITIZED);

        for(Search = 0 ; Search < OS.NumberOfTasks ; ++Search)
            if(*(OS.TaskList + Search) == pTask)
                break;
        if(Search != OS.NumberOfTasks)
        {
            OS.CurrentTaskNumber = Search;
            return 1;
        }
    }
	return 0;
}

static void CheckPipeTimeouts()
{
	LenuxPipe *pPipe;

	for(pPipe = OS.Pipes ; pPipe ; pPipe = pPipe->Next)
	{
		if(pPipe->Time != PIPE_WAIT_INFINITE)
		{
			if(Timeout(&(pPipe->Timer),pPipe->Time))
				pPipe->pTask->Flags &= ~TASKFLAG_SUSPENDED;
		}
	}
	return;
}

static LenuxTask** GetNextPriorityListItems(size_t NumberOfItems)
{
    size_t Count;
    LenuxTask **pNewList;
    
    for(Count = 0 ; Count < (OS.AllocatedPriorityList + 1 - NumberOfItems) ; ++Count)
        if(OS.PriorityTasks[Count] == NULL)
            return OS.PriorityTasks + Count;
	

    if((pNewList = malloc(OS.AllocatedPriorityList * 2 * sizeof(LenuxTask*))) != NULL)
    {
        memcpy(pNewList, OS.PriorityTasks, OS.AllocatedPriorityList * sizeof(LenuxTask*));
        memset(pNewList + OS.AllocatedPriorityList, 0, OS.AllocatedPriorityList * sizeof(LenuxTask*));
        free(OS.PriorityTasks);
        OS.PriorityTasks = pNewList;
        OS.AllocatedPriorityList *= 2;

        for(; Count < (OS.AllocatedPriorityList + 1 - NumberOfItems) ; ++Count)
            if(OS.PriorityTasks[Count] == NULL)
                return OS.PriorityTasks + Count;
    }
    return NULL;
}

static LenuxTask** GetFirstPriorityListItem()
{
    LenuxTask **ppResult;
    
    //Make room at the end of the list, so that we then can move everything one step to the back
    if((ppResult = GetNextPriorityListItems(1)) != NULL)
    {
        while(ppResult != OS.PriorityTasks)
        {
            *ppResult = *(ppResult - 1);
            --ppResult;
        }
    }
    return ppResult;
}

static void RemoveFromPriorityList(LenuxTask *pTask)
{
    size_t Source, Destination;
    
    Destination = 0;
    for(Source = 0 ; Source < OS.AllocatedPriorityList ; ++Source)
    {
        if(OS.PriorityTasks[Source] != pTask)
        {
            OS.PriorityTasks[Destination] = OS.PriorityTasks[Source];
            ++Destination;
        }
    }
}

static unsigned int IsBoundEvent(LenuxEvent *pEvent)
{
    return (((unsigned int)pEvent) - ((unsigned int)&(pEvent->pTask->WaitEvent)));
}

LenuxSwitchTaskReturn LenuxSwitchTask()
{
	LenuxSwitchTaskReturn Ret;
	LenuxEvent **ppEvent, *pEvent;
	LenuxTask **ppPriority;
	
	//Prioritize tasks waiting events that have been signaled. 
	for(ppEvent = &OS.pEvents ; *(ppEvent) != NULL ; )
	{
        pEvent = *ppEvent;
        
		if(     !(pEvent->pTask->Flags & (TASKFLAG_EVENT_SIGNALED | TASKFLAG_PRIORITIZED)) &&
                !((pEvent->pTask->Flags & TASKFLAG_WAITING_EVENT) && IsBoundEvent(pEvent)) )
		{
			if(pEvent->EventFunction())
			{
                pEvent->pTask->Flags &= ~TASKFLAG_SUSPENDED ;
                
                if((ppPriority = GetFirstPriorityListItem()) != NULL)
                {
                    *ppPriority = pEvent->pTask;
                    pEvent->pTask->Flags |= (TASKFLAG_EVENT_SIGNALED | TASKFLAG_PRIORITIZED);
				}
                
                //Remove event from list if it was a WaitEvent() call
                if(IsBoundEvent(pEvent) == 0)
                {
                    *ppEvent = pEvent->pNext;
                    continue;
                }
			}
		}
        
        ppEvent = &((*ppEvent)->pNext);
	}
	
	if(SchedulePrioritizedTask() == 0)
	{
		do
		{
			if(++OS.RoundRobinTaskNumber >= OS.NumberOfTasks)
			{
				CheckPipeTimeouts();	//When restarting scheduling from the beginning of the task list seems to be a good idea of when to check for pipes that have timed out
				OS.RoundRobinTaskNumber = 0;
			}
		}while((*(OS.TaskList + OS.RoundRobinTaskNumber))->Flags & TASKFLAG_SUSPENDED);
        
        OS.CurrentTaskNumber = OS.RoundRobinTaskNumber;
	}
	
	ResetTimerHighResolution(&OS.Timer);
	
	Ret.pContextStorage = (*(OS.TaskList + OS.CurrentTaskNumber))->pContextStorage;
	Ret.StackPointer = (*(OS.TaskList + OS.CurrentTaskNumber))->StackPointer;
	return Ret;
}

LenuxSwitchTaskReturn LenuxGetCurrentTaskContext()
{
	LenuxSwitchTaskReturn Ret;
	
	if(OS.PriorityTasks != NULL)
		SchedulePrioritizedTask();
	
	ResetTimerHighResolution(&OS.Timer);
	
	Ret.pContextStorage = (*(OS.TaskList + OS.CurrentTaskNumber))->pContextStorage;
	Ret.StackPointer = (*(OS.TaskList + OS.CurrentTaskNumber))->StackPointer;
	return Ret;
}

static void LenuxDeleteCurrentTask()
{
	unsigned short Index;
	LenuxTask *pTask;
	LenuxEvent **ppEvent, *pEvent;
	
	pTask = *(OS.TaskList + OS.CurrentTaskNumber);

	for(ppEvent = &OS.pEvents ; (*ppEvent) != NULL ; )
	{
		if((*ppEvent)->pTask == pTask) // If the event is connected to the task that is deleted
		{
			pEvent = *ppEvent;
			*ppEvent = (*ppEvent)->pNext;

			if(IsBoundEvent(pEvent))	//This should always be true, since a task that is waiting for an event can't terminate
				free(pEvent);
		}
		else
			ppEvent = &(*ppEvent)->pNext;
	}

	RemoveFromPriorityList(pTask);

	for(Index = OS.CurrentTaskNumber ; (Index + 1) < OS.NumberOfTasks ; ++Index)
		*(OS.TaskList + Index) = *(OS.TaskList + Index + 1);
	OS.NumberOfTasks--;
	
	if(pTask->pContextStorage != NULL)
		free(pTask->pContextStorage);
	free(pTask);
	
	if(OS.CurrentTaskNumber >= OS.NumberOfTasks)
		OS.CurrentTaskNumber = 0;
	
	while((*(OS.TaskList + OS.CurrentTaskNumber))->Flags & TASKFLAG_SUSPENDED)
	{
		if(++OS.CurrentTaskNumber >= OS.NumberOfTasks)
			OS.CurrentTaskNumber = 0;
	}
	
	MultiTask();
	return;
}

void ReleaseContextStorage()
{
	LenuxTask *pTask;
	pTask = *(OS.TaskList + OS.CurrentTaskNumber);
	if(pTask->pContextStorage != NULL)
		free(pTask->pContextStorage);
	pTask->pContextStorage = NULL;
	pTask->ContextSize = 0;
	return;
}

static int LenuxSetPrioritizedTask(LenuxTask *pTask)
{
    LenuxTask **ppPriorityList;
    
    if((ppPriorityList = GetNextPriorityListItems(2)) != NULL)
    {
        *ppPriorityList = *(OS.TaskList + OS.CurrentTaskNumber);
        *(ppPriorityList + 1) = pTask;
        pTask->Flags |= TASKFLAG_PRIORITIZED;
	
    	return 0;
    }
    else
        return -1;
}

int BindEvent(int (*pEventFunction)(void) )
{
	LenuxEvent *pEvent;
	
	if((pEvent = malloc(sizeof(LenuxEvent))) != NULL)
	{
		pEvent->EventFunction = pEventFunction;
		pEvent->pTask = *(OS.TaskList + OS.CurrentTaskNumber);
		pEvent->pNext = OS.pEvents;
		OS.pEvents = pEvent;
		
		return 0;
	}
	else
		return -1;
}

void WaitEvent(int (*pEventFunction)(void))
{
    LenuxTask *pTask;
    
    pTask = *(OS.TaskList + OS.CurrentTaskNumber);
    pTask->WaitEvent.EventFunction = pEventFunction;
    pTask->WaitEvent.pTask = pTask;
    pTask->WaitEvent.pNext = OS.pEvents;
    OS.pEvents = &pTask->WaitEvent;
    
    pTask->Flags |= (TASKFLAG_SUSPENDED | TASKFLAG_WAITING_EVENT);
    
    RemoveFromPriorityList(*(OS.TaskList + OS.CurrentTaskNumber));
    
    SwitchTask();
    pTask->Flags &= ~TASKFLAG_WAITING_EVENT;
}

int PipeListen(enum PipeIdentifiers Identifier,void *Destination,unsigned short Size,unsigned short Time)
{
	LenuxPipe **ppPipe,*pPipe;
	unsigned short WrittenSize;
	
	for( ppPipe = &OS.Pipes ; *ppPipe ; ppPipe = &(*ppPipe)->Next)
		if((*ppPipe)->Identifier == Identifier)
			return -2;
	
	pPipe = OS.pAllocatedPipes[Identifier];
	if(pPipe == NULL)
	{
		pPipe = malloc(sizeof(LenuxPipe));
		if(pPipe == NULL)
			return -1;
		OS.pAllocatedPipes[Identifier] = pPipe;
	}
	*ppPipe = pPipe;
	
	pPipe->Identifier = Identifier;
	pPipe->Size = Size;
	pPipe->Destination = Destination;
	pPipe->Time = Time;
	ResetTimer(&(pPipe->Timer));
	pPipe->pTask = *(OS.TaskList + OS.CurrentTaskNumber);
	
	pPipe->Next = NULL;
	
	pPipe->pTask->Flags |= TASKFLAG_SUSPENDED;
	
	LenuxPipeWait(pPipe);
	
	pPipe->pTask->Flags &= ~TASKFLAG_SUSPENDED;
	
	WrittenSize = Size - pPipe->Size;
	
	for(ppPipe = &OS.Pipes ; *ppPipe ; ppPipe = &((*ppPipe)->Next))
		if((*ppPipe) == pPipe)
		{
			*ppPipe = pPipe->Next;
			break;
		}
	return WrittenSize;
}

int PipeWrite(enum PipeIdentifiers Identifier,const void *Source, unsigned short Size)
{
	LenuxPipe **pPipe;
	
	for(pPipe = &OS.Pipes ; *pPipe ; pPipe = &(*pPipe)->Next)
	{
		if((*pPipe)->Identifier == Identifier)
		{
			if((*pPipe)->Size >= Size)
			{
				memcpy((*pPipe)->Destination,Source,Size);
				(*pPipe)->Destination += Size;
				(*pPipe)->Size -= Size;
				if((*pPipe)->Size == 0)
				{
					LenuxSetPrioritizedTask((*pPipe)->pTask);
					SwitchTask();
				}
				return 0;	
			}
			else
				return -2;
		}
	}
	return -1;
}

void InitLenux()
{
	LenuxPrepareStackForProfiling();
	
	memset(&OS,0x00, sizeof(OS));
	OS.TaskList = malloc(INITIAL_TASK_LIST_SIZE * sizeof(LenuxTask**));
	OS.AllocatedTaskList = INITIAL_TASK_LIST_SIZE;
	
	OS.PriorityTasks = malloc(INITIAL_TASK_LIST_SIZE * sizeof(LenuxTask*));
	OS.AllocatedPriorityList = INITIAL_TASK_LIST_SIZE;
	memset(OS.PriorityTasks, 0x00, INITIAL_TASK_LIST_SIZE * sizeof(LenuxTask*));
	
	InitLenuxTimer();
	return;
}

static volatile Timer_t Timer1mS;
static uint32_t HighResolutionFactor;

void ResetTimer(Timer_t *pTimer)
{
	__disable_irq();
	*pTimer = Timer1mS;
	__enable_irq();
	return;
}

static HighResolutionTimer_t ReadTimerHighResolution()
{
	HighResolutionTimer_t Timer;

	__disable_irq();

	Timer.Low = ((SYSTEM_FOSC / 1000) - 1) - SysTick->VAL;
	Timer.High = Timer1mS;

	if(SCB->ICSR & 0x04000000) //Check the PEND_STSET-bit
	{
		++Timer.High;
		Timer.Low = ((SYSTEM_FOSC / 1000) - 1) - SysTick->VAL;
	}

	__enable_irq();

	return Timer;
}

void ResetTimerHighResolution(HighResolutionTimer_t *pTimer)
{
	*pTimer = ReadTimerHighResolution();
	return;
}

uint32_t ElapsedTimeHighResolution(HighResolutionTimer_t Timer)
{
	HighResolutionTimer_t Now;
	uint32_t Result;
	
	Now = ReadTimerHighResolution();
	
	Result = Now.High - Timer.High;
	if(Now.Low < Timer.Low)
		Result -= 1;

	return Result;
}

HighResolutionTimer_t ElapsedTimeHighResolutionEx(HighResolutionTimer_t Timer)
{
    HighResolutionTimer_t Now;
    uint32_t Fixed;

    Now = ReadTimerHighResolution();

    Timer.High = Now.High - Timer.High;
    Fixed = Now.Low - Timer.Low;
    
    if(Now.Low < Timer.Low)
    {
        --Timer.High;
        Fixed += (SYSTEM_FOSC / 1000);
    }
   
    Fixed *= HighResolutionFactor;
    Timer.Low = Fixed;
 
    return Timer;
}

unsigned int TimeoutHighResolution(HighResolutionTimer_t *pTimer, unsigned int mS)
{
	if(ElapsedTimeHighResolution(*pTimer) >= mS)
	{
		pTimer->High += mS;
		return 1;
	}
	return 0;
}


unsigned int ElapsedTime(Timer_t Timer)
{
	return Timer1mS - Timer;
}

unsigned int GetTimeSinceLastCheck(Timer_t *Timer)
{
	unsigned int Time;
	Timer_t Now;
	
	ResetTimer(&Now);
	Time = Now - *Timer;
	*Timer += Time;
	return Time;
}

unsigned int Timeout(Timer_t *pTimer,unsigned int mS)
{
	Timer_t Now;

	ResetTimer(&Now);
	if((Now - *pTimer) >= mS)
	{
		*pTimer = Now;
		return 1;
	}
	return 0;
}	

void Delay(unsigned int mS)
{
	Timer_t StartTime, Now;
	
	ResetTimer(&StartTime);
	
	do
	{
		SwitchTask();
		ResetTimer(&Now);
	} while((Now - StartTime) < mS);
	return;
}


void SysTick_Handler(void)
{
	++Timer1mS;
}

void FakePassedTime(unsigned int Time)
{
	Timer1mS += Time;
	return;
}

void InitLenuxTimer()
{
	Timer1mS = 0;

	SysTick->LOAD = (SYSTEM_FOSC / 1000) - 1;
	SysTick->VAL = (SYSTEM_FOSC / 1000) - 1;
	SysTick->CTRL = 0x00000007;	// Clock source = Processor clock (AHB), Interrupt enabled, Counter enabled;

    HighResolutionFactor = 0x80000000UL / (SYSTEM_FOSC / 1000);
    HighResolutionFactor *= 2;
	return;
}
