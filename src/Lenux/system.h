#ifndef OS_SYSTEM_H
#define OS_SYSTEM_H

#define SYSTEM_FOSC	72000000

#include <stddef.h>

#define INITIAL_CONTEXT_STORAGE_SIZE		64	//Must not be set below 52!
#define INITIAL_TASK_LIST_SIZE				16

//Add the identifier for each pipe that will be used in this list:
enum PipeIdentifiers { 	CANOpenSDOServerPipe,
						CANOpenSDOClientPipe,
						FunctionChosenPipe,
						CalibrationBeepPipe,
						RPCPipe1,RPCPipe2,RPCPipe3,RPCPipe4,RPCPipe5,RPCPipe6,RPCPipe7,RPCPipe8,
						RPCPipe9,RPCPipe10,RPCPipe11,RPCPipe12,RPCPipe13,RPCPipe14,RPCPipe15,RPCPipe16,
						ErrorCodeNotificationPipe,
						RxPDOPipe,
						CAN2ReceivePipe,
						LastPipeIdentifier //Not used for a pipe, but needs to be the last element in this list.
						};


#define INITIAL_SDO_HANDLER_LIST_SIZE	12
#define SDO_HANDLER_LIST_SIZE_INCREMENT	2

#endif//OS_SYSTEM_H
