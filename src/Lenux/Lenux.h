#ifndef PIC24OS_H
#define PIC24OS_H

#include "system.h"
#include <stdint.h>

#ifndef SYSTEM_FOSC
#error SYSTEM_FOSC needs to be defined in system.h
#endif

#ifndef INITIAL_CONTEXT_STORAGE_SIZE
#error INITIAL_CONTEXT_STORAGE_SIZE needs to be defined in system.h
#endif

#ifndef INITIAL_TASK_LIST_SIZE
#error INITIAL_TASK_LIST_SIZE needs to be defined in system.h
#endif

typedef struct _LenuxPerformance
{
	unsigned short TotalNumberOfTasks;
	unsigned short AllocatedTaskListSize;
	unsigned short NumberOfSuspendedTasks;
	unsigned short NumberOfActivePipes;
	unsigned short TotalAllocatedContextStorage;
	unsigned short TotalUsedContextStorage;
	unsigned short MaximumStackUsage;
}LenuxPerformance;

typedef struct _HighResolutionTimer
{
	union
	{
		struct
		{
			uint32_t Low;
			uint32_t High;
		};
		uint64_t DWORD;
	};
}HighResolutionTimer_t;

typedef uint32_t Timer_t;

typedef void (*Task)(unsigned int Parameter);

#define PIPE_WAIT_INFINITE	0xffff

void InitLenux();
int CreateTask(Task pTaskEntry, unsigned int Parameter);
int CreateTaskEx(Task pTaskEntry, unsigned int Parameter, int ImmediateInvoke);
void SwitchTask();
void ReleaseContextStorage();
int BindEvent(int (*pEventFunction)(void) );
void WaitEvent(int (*pEventFunction)(void));
void MultiTask() __attribute__((noreturn));
int PipeListen(enum PipeIdentifiers Identifier, void *Destination, unsigned short Size, unsigned short Time);
int PipeWrite(enum PipeIdentifiers Identifier, const void *Source, unsigned short Size);
void QueryLenuxPerformance(LenuxPerformance *pPerformance);
void* CreateLenuxProfilingInformation(size_t *pSize);

void Delay(unsigned int mS);
void ResetTimer(Timer_t *pTimer);
unsigned int ElapsedTime(Timer_t Timer);
void ResetTimerHighResolution(HighResolutionTimer_t *pTimer);
uint32_t ElapsedTimeHighResolution(HighResolutionTimer_t Timer);
HighResolutionTimer_t ElapsedTimeHighResolutionEx(HighResolutionTimer_t Timer);
unsigned int GetTimeSinceLastCheck(Timer_t *pTimer);
unsigned int Timeout(Timer_t *pTimer, unsigned int mS);
unsigned int TimeoutHighResolution(HighResolutionTimer_t *pTimer, unsigned int mS);
void FakePassedTime(unsigned int Time);

#endif
